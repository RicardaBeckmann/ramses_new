!################################################################
!################################################################
!################################################################
!################################################################
subroutine backup_sink(filename)
  use amr_commons
  use pm_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
  integer,parameter::tag=1135
  integer::dummy_io,info2
#endif

  character(LEN=80)::filename

  integer::ilun,idim,i
  character(LEN=80)::fileloc
  character(LEN=5)::nchar
  real(dp),allocatable,dimension(:)::xdp
  integer,allocatable,dimension(:)::ii
  logical,allocatable,dimension(:)::nb

  if(.not. sink) return

  if(verbose)write(*,*)'Entering backup_sink'

  ilun=4*ncpu+myid+10

  call title(myid,nchar)
  fileloc=TRIM(filename)//TRIM(nchar)

  ! Wait for the token
#ifndef WITHOUTMPI
  if(IOGROUPSIZE>0) then
     if (mod(myid-1,IOGROUPSIZE)/=0) then
        call MPI_RECV(dummy_io,1,MPI_INTEGER,myid-1-1,tag,&
             & MPI_COMM_WORLD,MPI_STATUS_IGNORE,info2)
     end if
  endif
#endif

  open(unit=ilun,file=TRIM(fileloc),form='unformatted')
  rewind(ilun)

  write(ilun)nsink
  write(ilun)nindsink
  if(nsink>0)then
     allocate(xdp(1:nsink))
     do i=1,nsink
        xdp(i)=msink(i)
     end do
     write(ilun)xdp ! Write sink mass
     do i=1,nsink
        xdp(i)=tsink(i)
     end do
     write(ilun)xdp ! Write sink birth epoch
     do idim=1,ndim
        do i=1,nsink
           xdp(i)=xsink(i,idim)
        end do
        write(ilun)xdp ! Write sink position
     enddo
     do idim=1,ndim
        do i=1,nsink
           xdp(i)=vsink(i,idim)
        end do
        write(ilun)xdp ! Write sink velocity
     enddo
     do idim=1,ndim
        do i=1,nsink
           xdp(i)=lsink(i,idim)
        end do
        write(ilun)xdp ! Write sink angular momentum
     enddo
     do i=1,nsink
        xdp(i)=delta_mass(i)
     end do
     write(ilun)xdp ! Write sink accumulated rest mass energy
     deallocate(xdp)
     allocate(ii(1:nsink))
     do i=1,nsink
        ii(i)=idsink(i)
     end do
     write(ilun)ii ! Write sink index
     !        do i=1,nsink
     !           ii(i)=level_sink(i)
     !        end do
     !        write(ilun)ii ! Write sink level
     deallocate(ii)
     !        write(ilun)ncloud_sink   ! Write ncloud
     allocate(nb(1:nsink))
     do i=1,nsink
        nb(i)=new_born(i)
     end do
     write(ilun)nb ! Write level at which sinks where integrated
     deallocate(nb)
     write(ilun)sinkint_level ! Write level at which sinks where integrated
  endif
  close(ilun)

  ! Send the token
#ifndef WITHOUTMPI
  if(IOGROUPSIZE>0) then
     if(mod(myid,IOGROUPSIZE)/=0 .and.(myid.lt.ncpu))then
        dummy_io=1
        call MPI_SEND(dummy_io,1,MPI_INTEGER,myid-1+1,tag, &
             & MPI_COMM_WORLD,info2)
     end if
  endif
#endif

end subroutine backup_sink
!################################################################
!################################################################
!################################################################
!################################################################
subroutine output_sink(filename)
  use amr_commons
  use pm_commons
  implicit none
  character(LEN=80)::filename

  integer::isink
  integer::nx_loc,ilun
  real(dp)::scale,l_abs,rot_period,dx_min
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,scale_m
  character(LEN=80)::fileloc

  if(verbose)write(*,*)'Entering output_sink'

  ilun=myid+10

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  scale_m=scale_d*scale_l**3d0
  nx_loc=(icoarse_max-icoarse_min+1)
  scale=boxlen/dble(nx_loc)
  dx_min=scale*0.5D0**nlevelmax/aexp

  if(verbose)write(*,*)'Entering output_sink'

  ilun=2*ncpu+myid+10

  fileloc=TRIM(filename)
  open(unit=ilun,file=TRIM(fileloc),form='formatted',status='replace')
  !======================
  ! Write sink properties
  !======================
  write(ilun,*)'Number of sink = ',nsink

  write(ilun,'(" ============================================================================================================================================= ")')
  write(ilun,'("        Id       Mass(Msol)             x                y                z               vx               vy               vz         rsink_refine [code_length]")')
  write(ilun,'(" ============================================================================================================================================= ")')

  do isink=1,nsink
     l_abs=max((lsink(isink,1)**2+lsink(isink,2)**2+lsink(isink,3)**2)**0.5,1.d-50)
     rot_period=32*3.1415*msink(isink)*(dx_min)**2/(5*l_abs+tiny(0.d0))
     write(ilun,'(I10,8(2X,E15.7))')idsink(isink),msink(isink)*scale_m/2d33,xsink(isink,1:ndim),vsink(isink,1:ndim),rsink_refine
  end do
  write(ilun,'(" ================================================================================================================================== ")')
  close(ilun)

end subroutine output_sink
!################################################################
!################################################################
!################################################################
!################################################################
subroutine output_sink_csv(filename)
  use amr_commons
  use pm_commons
  implicit none
  character(LEN=80)::filename,fileloc

  integer::ilun,isink

  if(verbose)write(*,*)'Entering output_sink_csv'

  ilun=2*ncpu+myid+10

  fileloc=TRIM(filename)
  open(unit=ilun,file=TRIM(fileloc),form='formatted',status='replace', recl=500)
  !======================
  ! Write sink properties
  !======================
  do isink=1,nsink
     write(ilun,'(I10,12(A1,ES20.10))')idsink(isink),',',msink(isink),&
          ',',xsink(isink,1),',',xsink(isink,2),',',xsink(isink,3),&
          ',',vsink(isink,1),',',vsink(isink,2),',',vsink(isink,3),&
          ',',lsink(isink,1),',',lsink(isink,2),',',lsink(isink,3),&
          ',',t-tsink(isink),',',dMBHoverdt(isink)
  end do

  close(ilun)

end subroutine output_sink_csv
!################################################################
!################################################################
!################################################################
!################################################################
subroutine create_sink_fine_file(idsink_loc)
  use pm_commons
  use amr_commons
  implicit none

  !===========================================
  !This routine creates the SINKFILE filled at
  !each fine timestep using write_sink_fine
  !===========================================
  integer::idsink_loc
  character(LEN=80)::filename,filedir,filecmd
  integer::ilun,info,i
  character(LEN=5)::nchar

  filedir='SINK'
  if(sink.and.myid.eq.1)then
     filecmd='mkdir -p '//TRIM(filedir)
#ifdef NOSYSTEM
     call PXFMKDIR(TRIM(filedir),LEN(TRIM(filedir)),O'755',info)
#else
     call system(filecmd)
#endif
     ilun=4*ncpu+10+idsink_loc
     call title(idsink_loc,nchar)
     write(*,*)filedir,idsink_loc,ilun,nchar
     filename='SINK/sink_'//TRIM(nchar)//'.txt'
     open(unit=ilun,file=TRIM(filename),form='formatted',status='unknown',position='append')
     write(ilun,*)'#1      nstep'
     write(ilun,*)'#2      time [yr]'
     write(ilun,*)'#3      aexp'
     write(ilun,*)'#4      msink [Msun]'
     write(ilun,*)'#5      xsink 3 [pc]'
     write(ilun,*)'#6      vsink 3 [km/s]'
     write(ilun,*)'#7      lsink 3'
     write(ilun,*)'#8      dotMacc [Msun/yr]'
     write(ilun,*)'#9      dotMedd [Msun/yr]'
  endif

end subroutine create_sink_fine_file
!################################################################     
!################################################################     
!################################################################     
!################################################################     
subroutine write_sink_fine
  use pm_commons
  use amr_commons
  use hydro_commons
  implicit none
  
!==============================================
! This routine prints sink information on each
! fine timestep to follow black hole evolution
! in more detail 
!=============================================

  integer::isink,ilun
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,scale_m
  real(dp)::unit_amu,unit_pc,unit_msun,unit_dotM

  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  scale_m=scale_d*scale_l**3
  unit_amu=1.660538921e-24
  unit_pc=3.08567758096d18   !Possibly 3.08d18?                                                                                                                     
  unit_msun=1.98841586d33
  unit_dotM=(scale_m/scale_t)/unit_msun*3600*24*365

  do isink=1,nsink
     if(myid.eq.1)then
        ilun=4*ncpu+10+idsink(isink)
        write(ilun,42)nstep,aexp,t*scale_t/(3600*24*365.25),msink(isink)*scale_m/unit_msun,xsink(isink,:)*scale_l/unit_pc,vsink(isink,:)*scale_v/1d5,lsink(isink,:),dMsink_overdt(isink)*unit_dotM,dMEDoverdt(isink)*unit_dotM
        endif
     enddo
42 format((i8,1x,f23.15,1x),26(e23.15,1x))
end subroutine write_sink_fine
